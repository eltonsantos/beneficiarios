import React from "react";
import ReactDOM from "react-dom";

import "./styles/main.css";

import App from "./App";
// import { AdminProvider } from './hooks/admin';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
